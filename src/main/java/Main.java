import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Main {
    private static final String URL = "https://osu.ppy.sh/api/get_user?u=rafis&k=3e5accb0bcd2c2d7f6a9801b3d922d39ca8f26b0";

    public static void main(String[] args) throws IOException {
        CloseableHttpClient build = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(URL);
        CloseableHttpResponse response = build.execute(httpGet);
        HttpEntity entity = response.getEntity();
        String responseBody = EntityUtils.toString(entity, "UTF-8");

        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        User[] users = mapper.readValue(responseBody, User[].class);
        User userData = users[0];

        System.out.println(userData.getUserId() + " - " + userData.getUsername() + " - " + userData.getPpRaw());
    }
}
