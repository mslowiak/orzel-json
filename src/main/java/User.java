import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
    @JsonProperty("user_id")
    private int userId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("pp_raw")
    private double ppRaw;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getPpRaw() {
        return ppRaw;
    }

    public void setPpRaw(double ppRaw) {
        this.ppRaw = ppRaw;
    }
}
